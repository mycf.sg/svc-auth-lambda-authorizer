import {APIGatewayRequestAuthorizerHandler} from 'aws-lambda';
import {KMS} from 'aws-sdk';
import fetch from 'node-fetch';
import proxyAgent from 'proxy-agent';

export const handler: APIGatewayRequestAuthorizerHandler = async (event) => {
  let serviceGUID;
  for (const key in event.headers || {}) {
    if (/^x-api-key$/i.test(key)) {
      serviceGUID = event.headers?.[key];
      break;
    }
  }
  const svcAuthLoginURL = process.env.SVC_AUTH_LOGIN_URL;
  const proxyUrl = process.env.PROXY_URL;
  try {
    if (!serviceGUID) {
      throw new Error('missing dependencies: serviceGUID');
    }
    if (!svcAuthLoginURL) {
      throw new Error('missing dependencies: svcAuthLoginURL');
    }
    if (!event.stageVariables?.['kms_alias']) {
      throw new Error('missing dependencies: kms keyId');
    }

    const nonce = Date.now().toString();
    const payload = `${serviceGUID}${nonce}`;
    const signature = await signPayload(payload, event.stageVariables?.['kms_alias']);

    const accessToken = await fetch(svcAuthLoginURL, {
      agent: proxyUrl ? new proxyAgent(proxyUrl) : undefined,
      headers: {
        username: serviceGUID,
        signature,
        nonce,
      },
    }).then(async (response) => {
      const responseText = await response.text();
      if (!response.ok) {
        throw new Error(`unable to resolve access token: ${responseText}`);
      }
      return responseText;
    });
    return generateAllow(serviceGUID, event.methodArn, `Bearer ${accessToken}`);
  } catch (error) {
    console.error('Error getting service auth token', error);
    return generateDeny(serviceGUID || '', event.methodArn);
  }
};

const signPayload = async (payload: string, keyId: string) => {
  const kms = new KMS();
  const {Signature: signature} = await kms
    .sign({
      KeyId: keyId,
      Message: payload,
      SigningAlgorithm: 'RSASSA_PKCS1_V1_5_SHA_256',
    })
    .promise();
  if (!signature) {
    throw new Error('unable to get signature');
  }
  return signature.toString('base64');
};

const generatePolicy = (
  principalId: string,
  effect: 'Allow' | 'Deny',
  resource: string,
  context: {[key: string]: string | number | boolean} = {},
) => {
  // Required output:
  const authResponse = {
    principalId,
    policyDocument: {
      Version: '2012-10-17',
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: effect,
          Resource: resource,
        },
      ],
    },
    context,
  };
  return authResponse;
};

const generateAllow = (principalId: string, resource: string, accessToken: string) => {
  return generatePolicy(principalId, 'Allow', resource, {accessToken});
};

const generateDeny = (principalId: string, resource: string) => {
  return generatePolicy(principalId, 'Deny', resource);
};
