# svc-auth-lambda-authorizer

API Gateway Lambda authorizer to control access to our API. When a client calls the API thru API Gateway, API Gateway invokes this Lambda function to get the access-token from svc-auth before forwarding the request with the token.

Signing of the avc-auth request is done with a public key stored in KMS.
## workflow

```mermaid
graph TD
  client[Client]-->|1. request w/ api-key|api-gateway[API Gateway]
  api-gateway-->|2. invoke|lambda[Lambda Auth].->|3. policy + access-token|api-gateway
  api-gateway-->|4|evaluate-policy{policy is evaluated}
  evaluate-policy-->|5. Denied|client
  evaluate-policy-->|5. Allowed|api[[Internal APIs <br> base on the request and access-token permission/scope <br> will response accordingly]]
  api-->|6. Response|client

```

## Configuration

The following are environment variables

| Variable           | Description                                                               | Type   | Required |
| ------------------ | ------------------------------------------------------------------------- | ------ | -------- |
| SVC_AUTH_LOGIN_URL | svc-auth login endpoint to get the token                                  | string | Yes      |
| PROXY_URL          | Proxy to for svc-auth login, if svc-auth login in not directly accessible | string | No       |

### Setup

1. create asymmetric key pairs for each api-gateway api-key. [Instruction to create](https://gitlab.ci.mcf.sh/wsg/svc-auth#creating-new-asymmetric-key-pairs-for-new-services)
2. create a svc-auth service yaml configuration file [sample](https://gitlab.ci.mcf.sh/wsg/svc-auth#services-yaml-configuaration). `api-key` will be the service `guid`
3. deploy the service yaml configuration with svc-auth
